import * as React from 'react';
import {Fragment} from 'react';
import Row from 'react-grid-system/build/grid/Row';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {toggleIngredientDescription} from '../../actions/ingredient-actions';
import {connect} from 'react-redux';
import './ingredient.scss';

class Ingredient extends React.Component {

    render = () => {
        const {imgSrc, name, description, isExpanded, zIndex, id} = this.props.ingredient;
        return (
            <Fragment>
                <Row className={this.getPrimaryStyle(isExpanded)}>
                    <div className={'ingredient-container-result-item-primary'}
                         style={{zIndex: zIndex}}
                         onClick={() => this.handleClickExpand(id)}>
                        <div className={'ingredient-container-result-item-primary-logo'}>
                            {this.renderImgOrIcon(imgSrc, name)}
                        </div>
                        <div className={'ingredient-container-result-item-primary-info'}>
                            <div className={'ingredient-container-result-item-primary-info-name'}>
                                {name}
                            </div>
                            <div className={'ingredient-container-result-item-primary-info-actions'}>
                                <FontAwesomeIcon
                                    className={'ingredient-container-result-item-primary-info-actions-item-add'}
                                    icon={['fas', 'plus-square']}/>
                            </div>
                        </div>
                    </div>
                </Row>
                <Row className={this.getDescriptionStyle(isExpanded)}>
                    <div className={'ingredient-container-result-item-description'}>
                        {description}
                    </div>
                </Row>
            </Fragment>
        );
    };

    getPrimaryStyle = (isExpanded) => {
        const primaryStyle = 'ingredient-container-result-item-primary-transition';
        return isExpanded === false ? `${primaryStyle} ${primaryStyle}-hidden` : primaryStyle;
    }

    getDescriptionStyle = (isExpanded) => {
        const descriptionStyle = 'ingredient-container-result-item-description-transition';
        return isExpanded ? `${descriptionStyle} ${descriptionStyle}-hidden` : `${descriptionStyle}-hidden`;
    }

    renderImgOrIcon = (imgSrc, name) => {
        return imgSrc ? (
                <img className={'ingredient-container-result-item-primary-logo-img'}
                     src={imgSrc}
                     alt={name}/>
            )
            : (
                <FontAwesomeIcon className={'ingredient-container-result-item-primary-logo-alt'}
                                 icon={['fas', 'camera']}/>
            );
    };

    handleClickExpand = (id) => {
        this.props.toggleIngredientDescription(id);
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggleIngredientDescription: (id) => dispatch(toggleIngredientDescription(id)),
    }
};

export default connect(null, mapDispatchToProps)(Ingredient);


