const production = process.env.PRODUCTION;

export const BACKEND_PROTOCOL = production ? process.env.BACKEND_PROTOCOL : 'http';
export const BACKEND_ADDRESS = production ? process.env.BACKEND_ADDRESS : 'localhost';
export const BACKEND_PORT = production ? process.env.BACKEND_PORT : '8080';
